const bcrypt = require("bcrypt");
const mongoose = require("mongoose");

const jwt = require("jsonwebtoken");
const {
  JWT_SECRET
} = require("../config");
const {
  User
} = require("../models/userModel");

module.exports.getNotes = async (req, res) => {
  const user = await User.findOne({
    _id: req.userData.id
  });

  res.status(200).json({
    "notes": user
  })

};

module.exports.addNotes = async (req, res) => {
  const {
    text
  } = req.body;

  if (!text) {
    return res.status(400).json({
      message: "You should specify task"
    })
  }

  const user = await User.findOne({
    _id: req.userData.id
  });

  user.text = text;
  user.completed = false
  await user.save();

  res.status(200).json({
    message: "Success"
  })
}

module.exports.getNotesById = async (req, res) => {
  const user = await User.findById(req.params.id);
  if (!user) {
    return res.status(400).json({
      message: `You specified wrong id`
    })
  }

  res.status(200).json({
    "note": user
  });
}

module.exports.putNotesById = async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user) {
    return res.status(400).json({
      message: `You specified wrong id`
    })
  }

  const {
    text
  } = req.body;

  if (!text) {
    return res.status(400).json({
      message: "You should specify task"
    })
  }

  user.text = text;
  user.completed = false;
  await user.save();

  res.status(200).json({
    message: "Success"
  })
}

module.exports.checkNotesById = async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user) {
    return res.status(400).json({
      message: `You specified wrong id`
    })
  }

  user.completed = user.completed === false ? true : false;
  await user.save();

  res.status(200).json({
    message: "Success"
  })
}

module.exports.checkNotesById = async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user) {
    return res.status(400).json({
      message: `You specified wrong id`
    })
  }

  user.completed = user.completed === false ? true : false;
  await user.save();

  res.status(200).json({
    message: "Success"
  })
}

module.exports.deleteNotesById = async (req, res) => {
  const user = await User.findByIdAndDelete(req.params.id);

  if (!user) {
    return res.status(400).json({
      message: `You specified wrong id`
    })
  }

  res.status(200).json({
    message: "Succes"
  })
}