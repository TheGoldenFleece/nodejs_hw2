const bcrypt = require("bcrypt");
const mongoose = require("mongoose");

const jwt = require("jsonwebtoken");
const {
  JWT_SECRET
} = require("../config");
const {
  User
} = require("../models/userModel");

module.exports.getNotes = async (req, res) => {
  const user = await User.findOne({
    _id: req.userData.id
  });

  res.status(200).json({
    user: { _id: user._id, username: user.username, createdDate: user.createdDate }
  })

};

module.exports.getUserInfo = async (req, res) => {
  const user = await User.findOne({
    _id: req.userData.id
  });

  res.status(200).json({
    "user": {
      "_id": user._id,
      "username": user.username,
      "createdDate": user.createdDate
    }
  })
}

module.exports.deleteUser = async (req, res) => {

  const user = await User.findOneAndDelete({
    _id: req.userData.id
  });

  res.status(200).json({
    message: "Succes"
  })
}


module.exports.changePassword = async (req, res) => {
  const {
    oldPassword,
    newPassword
  } = req.body;

  console.log(oldPassword, newPassword);

  const user = await User.findOne({
    _id: req.userData.id
  });

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(401).json({
      message: `Wrong password`
    })
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({
    message: "Success"
  })
}
