const mongoose = require('mongoose');



const userSchema = mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },

  userId: String,
  completed: Boolean,
  text: String,
  createdDate: {
    type: Date,
    default: Date.now()
  }
})

module.exports.User = mongoose.model('User', userSchema);