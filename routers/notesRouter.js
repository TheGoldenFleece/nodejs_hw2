const express = require('express');
const mongoose = require("mongoose");
const router = express.Router();
const jwt = require('jsonwebtoken');
const {
  JWT_SECRET
} = require('../config');



const {
  User
} = require('../models/userModel');

const {
  asyncWrapper
} = require("./helpers");

const {
  authMiddleware
} = require('./middlewares/authMiddleware');

const {
  getNotes,
  addNotes,
  getNotesById,
  putNotesById,
  checkNotesById,
  deleteNotesById
} = require("../controllers/notesController");


// get /api/notes Get user`s notes
router.get('/', authMiddleware, asyncWrapper(getNotes));

// post /api/notes Get user`s notes
router.post('/', authMiddleware, asyncWrapper(addNotes));

// get /api/notes get user`s notes by id
router.get('/:id', asyncWrapper(getNotesById));

// put /api/notes put user`s notes by id
router.put('/:id', asyncWrapper(putNotesById));

// patch /api/notes check/uncheck user`s notes by id
router.patch('/:id', asyncWrapper(checkNotesById));


// delete /api/notes delete user`s notes by id
router.delete('/:id', asyncWrapper(deleteNotesById));

module.exports = router;