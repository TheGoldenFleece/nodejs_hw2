const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');
const {
  JWT_SECRET
} = require('../config');


const {
  User
} = require('../models/userModel');

const {
  asyncWrapper
} = require("./helpers");

const {
  authMiddleware
} = require('./middlewares/authMiddleware');

const {
  getUserInfo,
  deleteUser,
  changePassword
} = require("../controllers/userController");


// get /api/users/me Get user`s profile info
router.get('/me', authMiddleware, asyncWrapper(getUserInfo));

// delete /api/users/me  delete user
router.delete('/me', authMiddleware, asyncWrapper(deleteUser));

// patch /api/users/me  change user`s password 
router.patch('/me', authMiddleware, asyncWrapper(changePassword));

module.exports = router;