const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const {
  asyncWrapper
} = require("./helpers");
const {
  validateRegistration
} = require("./middlewares/validationMiddleware");
const {
  login,
  registration
} = require("../controllers/authController");

// post /api/auth/register create user
router.post(
  "/register",
  asyncWrapper(validateRegistration),
  asyncWrapper(registration)
);

// post /api/auth/login get user`s token
router.post("/login", asyncWrapper(login));

module.exports = router;