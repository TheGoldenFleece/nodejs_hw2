const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");

const app = express();

const port = process.env.PORT || 8080;

const authRouter = require("./routers/authRouter");
const usersRouter = require("./routers/usersRouter");
const notesRouter = require("./routers/notesRouter");

app.use(express.json());
app.use(morgan("tiny"));

app.use("/api/auth", authRouter);
app.use("/api/users", usersRouter);
app.use("/api/notes", notesRouter);

class UnauthorizedError extends Error {
  constructor(message = 'Unauthorized user!') {
    super(message);
    statusCode = 401;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({
      message: err.message
    });
  }
  res.status(500).json({
    message: err.message
  });
});

const start = async () => {
  await mongoose.connect(
    "mongodb+srv://testuser:testuser@cluster0.orype.mongodb.net/myFirstDatabase", {

      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    }
  );

  console.log(process.env);

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();